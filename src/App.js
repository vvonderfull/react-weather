import React from "react";
import "./index.scss";
import { BrowserRouter, Switch, Route, NavLink } from "react-router-dom";
import Main from "./pages/Main";
import Favorite from "./pages/Favorite";

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <NavLink to="/favorite">GO</NavLink>
          <Switch>
            <Route path={"/"} exact component={Main} />
            <Route path={"/favorite"} component={Favorite} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
