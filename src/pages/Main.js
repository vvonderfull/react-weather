import React, { Fragment } from "react";

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      findCity: "",
      cities: [],
    };
  }
  updateFindCity(value) {
    this.setState({
      findCity: value,
    });
  }
  render() {
    return (
      <Fragment className="Main">
        <div className="find-container">
          <h1>Поиск городов:</h1>
          <input
            type="text"
            value={this.state.findCity}
            onChange={(e) => this.updateFindCity(e.target.value)}
          />
        </div>
      </Fragment>
    );
  }
}

export default Main;
